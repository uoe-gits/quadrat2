#!/usr/bin/env python3

from setuptools import setup

setup(name='quadrat2',
      install_requires = ['pyxattr',
                          'ldap3>=2.7',
                          'cephfs',
                          'rados',
#                          'posix1e',
                          'distro',
                      ],
      version='0.0.11',
      description='Quota, User and Directory Remote Administration Tool',
      author='Magnus Hagdorn',
      author_email='magnus.hagdorn@ed.ac.uk',
      url='https://git.ecdf.ed.ac.uk/uoe-gits/quadrat',
      packages=['quadrat2'],
      entry_points={
          'console_scripts': [
              'geos-cull = quadrat2.cull:main',
              'geos-homesmap = quadrat2.homesmap:main',
              'geos-volumes = quadrat2.volumes:main',
              'geos-snapshot = quadrat2.snapshot:main',
              'geos-manage-user-project = quadrat2.manage_user_projects:main',
              'quadrat2 = quadrat2.users:main',
              ],
      },

  )

