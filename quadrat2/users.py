__all__ = ['Users','CentralUsers','GeosUsers','User']

import argparse
import configparser
import pathlib

import logging
from .logging import *
from .configfiles import readUserVolumes
from .convert_size import convert_size
from .ldap import LDAPConnection
from . import __version__
import ldap3

log = logging.getLogger('quadrat2.users')

class Quotas:
    def __init__(self,cfg):
        self._quotas = {}

        for s in cfg.sections():
            if s.startswith('quota_'):
                qtype = s[6:]
                quota = {}
                for d,q in cfg.items(s):
                    quota[d] = q
                self._quotas[qtype] = quota
                
        assert 'default' in self._quotas

    def _get_qtype(self,qtype):
        if qtype in self._quotas:
            return qtype
        else:
            return 'default'

    def volumes(self,qtype):
        for v in self._quotas[self._get_qtype(qtype)]:
            yield v
            
    def quota(self,qtype,volume):
        return self._quotas[self._get_qtype(qtype)][volume]

    def quota_entry(self,qtype,volume):
        q = convert_size(self.quota(qtype,volume))
        return '{}:{},0,0,0'.format(volume,int(q/1024))

class Users:
    OBJ = None
    def __init__(self,ldap,base):
        self._ldap = ldap
        self._base = base
        if self.OBJ is None:
            raise NotImplementedError
        self._obj = ldap3.ObjectDef(self.OBJ, self.ldap.connection)
        
    @property
    def ldap(self):
        return self._ldap
    @property
    def base(self):
        return self._base
    @property
    def obj(self):
        return self._obj

    def __call__(self,uid):
        reader = ldap3.Reader(self.ldap.connection,self.obj,
                              'ou=People,'+self.base,'uid:={}'.format(uid))
        reader.search()
        if len(reader)<1:
            raise RuntimeError('no such user {}'.format(uid))
        return reader[0]
        
    @property
    def all_users(self):
        reader = ldap3.Reader(self.ldap.connection,self.obj,
                              'ou=People,'+self.base)
        reader.search()
        for u in reader:
            yield u

class CentralUsers(Users):
    OBJ = ['posixAccount','edEduPerson']
class GeosUsers(Users):
    OBJ = ['account','posixAccount']

class User:
    def __init__(self,ldap,base,user):
        self._ldap = ldap
        self._base = base
        self._uid = user.uid.value
        self._central_user = user
        self.user = self._get_user()

    @property
    def ldap(self):
        return self._ldap
    @property
    def base(self):
        return self._base
    @property
    def uid(self):
        return self._uid

    def _find_user(self,extraObjectClasses=[]):
        objectClass = ['account','posixAccount'] + extraObjectClasses

        obj = ldap3.ObjectDef(objectClass, self.ldap.connection)
        reader = ldap3.Reader(self.ldap.connection,obj,'ou=People,'+self.base,'uid:={}'.format(self._central_user.uid))
        reader.search()
        if len(reader)<1:
            return None
        return reader[0]

    def _get_user(self):
        # see if we have a full entry
        entry = self._find_user(extraObjectClasses=['systemQuotas'])
        if entry is None:
            # see if we have a partial entry
            entry = self._find_user()
            
            if entry is None:
                # create new user
                obj = ldap3.ObjectDef(['account','posixAccount','systemQuotas'], self.ldap.connection)
                writer = ldap3.Writer(self.ldap.connection, obj)
                entry = writer.new('uid={},ou=People,{}'.format(self._central_user.uid,self.base))

                for a in ['cn','gecos','gidNumber','homeDirectory','uid','uidNumber','loginShell']:
                    setattr(entry,a,self._central_user.entry_attributes_as_dict[a])
            else:
                # add system quotas
                entry = entry.entry_writable()
                entry.objectClass += 'systemQuotas'

            if entry.entry_status == "Pending changes":
                if self.ldap.confirm('{}\nmodify {} [y/N]: '.format(str(entry),self._central_user.uid)):
                    entry.entry_commit_changes()
                if self.ldap.connection.result['result'] != 0:
                    log.error("{0[description]} - {0[message]}".format(self.ldap.connection.result))
            # reload entry to make sure it is all up-to-date
            entry = self._find_user(extraObjectClasses=['systemQuotas'])

        return entry.entry_writable()

    @property
    def volumes(self):
        for q in self.user.quota.values:
            q = q.split(':')
            yield q[0]
            
    def get_quota(self,volume):
        for q in self.user.quota.values:
            q = q.split(':')
            if q[0] == volume:
                q = q[1].split(',')[0]
                return int(q)*1024
        return 0

    def commit(self):
        if self.user.entry_status == "Pending changes":
            if self.ldap.confirm('{}\nmodify {} [y/N]: '.format(str(self.user),self.uid)):
                self.user.entry_commit_changes()
            if self.ldap.connection.result['result'] != 0:
                log.error("{0[description]} - {0[message]}".format(self.ldap.connection.result))

    def set_quota(self,volume,size,shrink=False):
        size = int(convert_size(size)/1024.)
        for q in self.user.quota:
            if q.startswith(volume+':'):
                orig_volume, orig_value = q.split(':')
                orig_size = int(orig_value.split(',')[0])
                if orig_size==size:
                    log.debug('quota {} for use {} is unchanged'.format(volume,self.uid))
                    return
                if orig_size>size and not shrink:
                    log.info('not reducing quota {} for user {}'.format(volume,self.uid))
                    return                                
                self.user.quota -= q
        self.user.quota += '{}:{},0,0,0'.format(volume,size)

    def update_quota(self,quotas):
        qtype = self._central_user.eduniCategory.value
        for v in quotas.volumes(qtype):
            self.set_quota(v,quotas.quota(qtype,v))

    def info(self):
        log.info('{0.OA_uid}:*:{0.uidNumber}:{0.gidNumber}:{0.gecos}:{0.homeDirectory}:{0.loginShell}'.format(self.user))
        for v in self.volumes:
            log.info('quota:{}:{}:{}'.format(self.uid,v,convert_size(self.get_quota(v),inverse=True)))

def main():
    CFG = '/etc/sysconfig/quadrat2'

    parser = argparse.ArgumentParser(parents=[loggingParser()],
                                     prog='quadrat2',
                                     description='Manage user directories.')

    parser.add_argument('user',nargs='?',help='the user to be modified')

    group = parser.add_mutually_exclusive_group()
    group.add_argument('--update-users',
                       action='store_true',
                       default=False,
                       help="update all user entries")
    group.add_argument('-q','--quota',
                       action='append',
                       metavar='QTYPE[:SIZE]',
                       help="get/set the quota QTYPE. Set the quota when size is specified. When specifying sizes you can use the usual suffices eg G for gigabyte")
    group.add_argument('-i','--info',
                       action='store_true',
                       default=False,
                       help="print user information")

    control = parser.add_argument_group("Control program behaviour")
    control.add_argument('-k','--keytab', 
                         action="store_true", default=False,
                         help="use host keytab to get krb5 credentials")
    control.add_argument("-y", "--yes",
                         action="store_true", default=False,
                         help="Never prompt for confirmation.")
    control.add_argument("-n", "--dry-run", "--no",
                         action="store_true", default=False,
                         help="Display what would be done, but make no changes.")

    parser.add_argument('--version',action='version',version=__version__)

    parser.add_argument('-c','--config',default=CFG,help='read configuration from file, default={}'.format(CFG))

    args = parser.parse_args()

    set_up_logging(args)

    # configure
    config = configparser.SafeConfigParser()
    config.read(args.config)

    volumes = readUserVolumes(config)


    for ldap in ('ldap', 'base'):
        setattr(args, ldap, config.get('ldaplist', ldap))

    ceph_basedir = pathlib.Path(config.get('cephfs','basedir'))

    ldap = LDAPConnection(dry_run=args.dry_run,yes=args.yes)
    if args.keytab:
        ldap.kinit()
    ldap.connect(args.ldap)

    central = LDAPConnection()
    central.connect(config.get('ldaplist', 'centralldap'),anonymous=True)
    cbase = config.get('ldaplist', 'centralbase')
    cUsers = CentralUsers(central,cbase)

    quotas = Quotas(config)

    if args.update_users:
        gUsers = GeosUsers(ldap,args.base)
        for u in gUsers.all_users:
            try:
                cu = cUsers(u.OA_uid)
            except:
                log.warning('no such user {}'.format(u.OA_uid))
                continue
            user = User(ldap,args.base,cu)
            user.update_quota(quotas)
            user.commit()
    else:
        if args.user is None:
            parser.error('No user specified')

        # try to get the user from central auth
        try:
            cuser = cUsers(args.user)
        except:
            parser.error('No such user {}'.format(args.user))

        user = User(ldap,args.base,cuser)

        if args.quota is not None:
            for q in args.quota:
                q = q.split(':')
                if q[0] not in volumes:
                    parser.error('unknown quota type {}'.format(q[0]))
                if len(q) == 1:
                    v = user.get_quota(q[0])
                    print ('{} {} {}'.format(user.uid,q[0],convert_size(v,inverse=True)))
                else:
                    user.set_quota(q[0],q[1],shrink=True)
        elif args.info:
            user.info()
        else:
            user.update_quota(quotas)
        user.commit()

if __name__ == '__main__':
    main()
