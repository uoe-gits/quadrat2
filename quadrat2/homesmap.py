import argparse
import configparser
import pathlib

import sys
import logging
from .logging import *
from .ldap import LDAPConnection
from . import __version__
import ldap3

log = logging.getLogger('quadrat2.homesmap')

def list_volume_types(ldap,base):
    obj = ldap3.ObjectDef('nisMap', ldap.connection)
    reader = ldap3.Reader(ldap.connection,obj,base)
    reader.search()
    for i in reader:
        print (i.nisMapName)
def add_volume_type(ldap,base,volType):
    obj = ldap3.ObjectDef('nisMap', ldap.connection)
    reader = ldap3.Reader(ldap.connection,obj,base,'nisMapName:={}'.format(volType))
    reader.search()
    if len(reader)>0:
        log.error('nisMap {} already exists'.format(volType))
        sys.exit(1)
    writer = ldap3.Writer.from_cursor(reader)
    entry = writer.new('nisMapName={},{}'.format(volType,base))
    entry.nisMapName = volType
    if entry.entry_status == "Pending changes":
        if ldap.confirm('{}\nadd volume type {} [y/N]: '.format(str(entry),volType)):
            entry.entry_commit_changes()
        if ldap.connection.result['result'] != 0:
            log.error("{0[description]} - {0[message]}".format(ldap.connection.result))
def delete_volume_type(ldap,base,volType):
    obj = ldap3.ObjectDef('nisMap', ldap.connection)
    reader = ldap3.Reader(ldap.connection,obj,base,'nisMapName:={}'.format(volType))
    reader.search()
    if len(reader) == 0:
        log.error('nisMap {} not found'.format(volType))
        sys.exit(1)
    nisObjects = ldap3.ObjectDef('nisObject', ldap.connection)
    rNisObjects = ldap3.Reader(ldap.connection,nisObjects,base,'nisMapName:={}'.format(volType))
    rNisObjects.search()
    if len(rNisObjects):
        log.error('nisMap {} contains {} nisObjects, not deleting'.format(volType,len(rNisObjects)))
        sys.exit(1)
    writer = ldap3.Writer.from_cursor(reader)
    for i in writer:
        if ldap.confirm('delete {0.nisMapName} [y/N]: '.format(i)):
            i.entry_delete()
    writer.commit()

def list_volumes(ldap,base,volType):
    obj = ldap3.ObjectDef('nisObject', ldap.connection)
    reader = ldap3.Reader(ldap.connection,obj,base,'nisMapName:={}'.format(volType))
    reader.search()
    for i in reader:
        print (i.cn, i.nisMapEntry)

def delete_volume(ldap,base,nismap,volume):
    obj = ldap3.ObjectDef('nisObject', ldap.connection)
    reader = ldap3.Reader(ldap.connection,obj,base,'cn:={},nisMapName:={}'.format(volume,nismap))
    reader.search()
    writer = ldap3.Writer.from_cursor(reader)
    for i in writer:
        if ldap.confirm('delete {0.cn} {0.nisMapEntry} from {0.nisMapName} [y/N]: '.format(i)):
            i.entry_delete()
    writer.commit()
    if ldap.connection.result['result'] != 0:
        log.error("{0[description]} - {0[message]}".format(ldap.connection.result))

def modify_volume_raw(ldap,base,nismap,volume,raw_entry):
    obj = ldap3.ObjectDef('nisObject', ldap.connection)
    reader = ldap3.Reader(ldap.connection,obj,base,'cn:={},nisMapName:={}'.format(volume,nismap))
    reader.search()
    writer = ldap3.Writer.from_cursor(reader)    
    if len(writer) == 0:
        # new entry
        entry = writer.new('cn={},nisMapName={},{}'.format(volume,nismap,base))
    else:
        entry = writer[0]
    if entry.nisMapName != nismap:
        entry.nisMapName = nismap
    if entry.nisMapEntry != raw_entry:
        entry.nisMapEntry = raw_entry
    if entry.entry_status == "Pending changes":
        if ldap.confirm('{}\nmodify {} [y/N]: '.format(str(entry),volume)):
            entry.entry_commit_changes()
        if ldap.connection.result['result'] != 0:
            log.error("{0[description]} - {0[message]}".format(ldap.connection.result))
    

def modify_volume(ldap,base,nismap,volume,host,path,root=None):
    assert isinstance(path,pathlib.Path)

    if root is None:
        target = path
    else:
        if path.is_absolute():
            path = pathlib.Path(*path.parts[1:])
        target = root/path
    target = '{}:{}'.format(host,target)
    modify_volume_raw(ldap,base,nismap,volume,target)

def main():
    CFG = '/etc/sysconfig/quadrat2'

    parser = argparse.ArgumentParser(parents=[loggingParser()],
                                     prog='homesmap',
                                     description='Manage server volume records in the LDAP.')
    parser.add_argument('vol',
                        nargs='?',
                        help='volume name to be created/modified/deleted.')


    group = parser.add_mutually_exclusive_group()
    group.add_argument('-t','--list-type',
                       action='store_true',
                       default=False,
                       help="list the volume types")
    group.add_argument('-l','--list-volumes',
                       action='store_true',
                       default=False,
                       help="list all volumes associated with a volume type. By default vol=auto.homes")
    group.add_argument("--add-type",
                       action='store_true',
                       default=False,
                       help="add volume type vol")
    group.add_argument("--delete-type",
                       action='store_true',
                       default=False,
                       help="delete volume type vol")
    group.add_argument("-m","--modify-volume",
                       action='store_true',
                       default=False,
                       help="modify volume vol")
    group.add_argument('-d','--delete-volume',
                       action='store_true',
                       default=False,
                       help="delete volume vol")

    nis = parser.add_argument_group("homesmap entries options")
    nis.add_argument("-M","--map",default="auto.homes",help="name of nisMap to be modified, default: auto.homes")
    nis.add_argument("-H","--host",help="name of NFS server")
    nis.add_argument("-p","--path",help="path to volume")
    nis.add_argument("-E","--entry",help="the raw nisMapEntry")

    control = parser.add_argument_group("Control program behaviour")
    control.add_argument('-k','--keytab', 
                         action="store_true", default=False,
                         help="use host keytab to get krb5 credentials")
    control.add_argument("-y", "--yes",
                         action="store_true", default=False,
                         help="Never prompt for confirmation.")
    control.add_argument("-n", "--dry-run", "--no",
                         action="store_true", default=False,
                         help="Display what would be done, but make no changes.")

    parser.add_argument('--version',action='version',version=__version__)

    parser.add_argument('-c','--config',default=CFG,help='read configuration from file, default={}'.format(CFG))

    args = parser.parse_args()

    set_up_logging(args)

    # configure
    config = configparser.SafeConfigParser()
    config.read(args.config)

    for ldap in ('ldap', 'base'):
        setattr(args, ldap, config.get('ldaplist', ldap))

    ldap = LDAPConnection(dry_run=args.dry_run,yes=args.yes)
    if args.keytab:
        ldap.kinit()
    ldap.connect(args.ldap)

    if args.list_type:
        list_volume_types(ldap,args.base)
    elif args.list_volumes:
        list_volumes(ldap,args.base,args.map)
    elif args.add_type:
        if args.map is None:
            parser.error ('Need to specify argument map')
        add_volume_type(ldap,args.base,args.map)
    elif args.delete_type:
        if args.map is None:
            parser.error ('Need to specify argument map')
        delete_volume_type(ldap,args.base,args.map)
    elif args.modify_volume:
        if args.vol is None:
            parser.error('Need to specify volume to be modified')
        if args.entry is not None:
            modify_volume_raw(ldap,args.base,args.map,args.vol,args.entry)
        else:
            for a in ['host','path']:
                if getattr(args,a) is None:
                    parser.error('Need to specify argument {}'.format(a))
            modify_volume(ldap,args.base,args.map,args.vol,args.host,
                          pathlib.Path(args.path))
    elif args.delete_volume:
        if args.vol is None:
            parser.error('need to specify volume to delete')
        delete_volume(ldap,args.base,args.map,args.vol)

if __name__ == '__main__':
    main ()
