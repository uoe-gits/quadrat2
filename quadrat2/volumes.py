__all__ = ['userVolumeName']

import argparse
import configparser
import pathlib,shutil,os
import pwd,grp
import xattr
import string
from math import log2
import ldap3

import logging
from .logging import *
from .ldap import LDAPConnection
from .homesmap import modify_volume
from .configfiles import readUserVolumes,readGroupVolumes
from .convert_size import convert_size
from . import __version__

log = logging.getLogger('quadrat2.volumes')

class Volume:
    def __init__(self,base,path,user,group,mode,size,uid=None,gid=None):
        self._base = pathlib.Path(base)
        self._path = pathlib.Path(path)
        self._user = user
        self._group = group
        if uid is None:
            self._uid = pwd.getpwnam(self.user).pw_uid
        else:
            self._uid = uid
        if gid is None:
            self._gid = grp.getgrnam(self.group).gr_gid 
        else:
            self._gid = gid
        self._mode = int(mode,base=8)
        self._size = size
        
        self._volume = None

    @property
    def base(self):
        return self._base
    @property
    def path(self):
        return self._path
    @property
    def user(self):
        return self._user
    @property
    def group(self):
        return self._group
    @property
    def uid(self):
        return self._uid
    @property
    def gid(self):
        return self._gid
    @property
    def mode(self):
        return self._mode
    @property
    def size(self):
        return self._size

    @property
    def volume(self):
        if self._volume is None:
            if self.path.is_absolute():
                p = pathlib.Path(*self.path.parts[1:])
            else:
                p = self.path
            self._volume = self.base/p
        return self._volume

    def create_parent(self):
        """create parent directories"""
        if not self.volume.parent.exists():
            log.debug('creating parent directory {}'.format(self.volume.parent))
            self.volume.parent.mkdir(parents=True)

    def _get_path(self,path=None):
        if path is not None:
            path = pathlib.Path(string.Template(path).substitute(user=self.user))
            if path.is_absolute():
                path = pathlib.Path(path.parts[1:])
            else:
                path = path
        else:
            path = ''
        return self.volume/path

    def _get_mode(self,mode=None):
        if mode is None:
            return self.mode
        else:
            return int(mode,base=8)

    def apply_size(self,path=None):
        path = self._get_path(path=path)
        quota = max(convert_size(self.size),1)
        try:
            current_quota = int(xattr.getxattr(str(path),"ceph.quota.max_bytes"))
        except:
            current_quota = None
        if current_quota != quota:
            log.info('changing size of {} to {}'.format(path,quota))
            xattr.setxattr(str(path),"ceph.quota.max_bytes",str(quota))

    def create_directory(self,path=None,mode=None):
        path = self._get_path(path=path)
        if path.exists():
            log.debug('directory {} already exists'.format(path))
            return

        self.create_parent()
        log.debug('creating directory {}'.format(path))
        path.mkdir()
        os.chown(path,self.uid,self.gid)
        path.chmod(self._get_mode(mode))

    def copy_directory(self,source,path=None,mode=None):
        path = self._get_path(path=path)
        if path.exists():
            log.debug('directory {} already exists'.format(path))
            return

        self.create_parent()
        log.debug('copying {} to {}'.format(source,path))
        shutil.copytree(source,path)
        path.chmod(self._get_mode(mode))
        os.chown(path,self.uid,self.gid)
        for p in path.rglob("*"):
            os.chown(p,self.uid,self.gid)

    def symlink(self,source,path):
        path = self._get_path(path=path)
        source = string.Template(source).substitute(user=self.user)
        if path.is_symlink() or path.exists():
            log.debug('directory {} already exists'.format(path))
            return
        
        log.debug('linking {} to {}'.format(source,path))
        path.symlink_to(source)
        os.chown(path,self.uid,self.gid,follow_symlinks=False)

class UserVolume(Volume):
    def __init__(self,base,path,user,group,mode,size,uid=None,gid=None):
        super().__init__(base,path,user,group,mode,size,uid=uid,gid=gid)
        self._path = userVolumeName(self.path,user)
    
    @property
    def is_staff(self):
        return _is_staff(self.user)
    @property
    def is_student(self):
        return not self.is_staff

def _is_staff(user):
    return not user[1:].isdigit()
def _is_student(user):
    return not _is_staff(user)
def userVolumeName(path,user):
    # Choose a suitable automatic subdirectory for this username
    # We mainly use the first character, unless that is the only non-digit
    # in the name, in which case we use the last character instead

    if _is_student(user):
        _hash = user[-1]
    else:
        _hash = user[0]

    return path/_hash/user

def createUserVolume(cfg,volume):
    #volume = UserVolume(base,cfg['_rootdir'][0],user,user,'750','16G')

    # create base directory
    if cfg['-'][0] == 'copy':
        volume.copy_directory(cfg['-'][1],mode=cfg['-'][2])
    elif cfg['-'][0] == 'dir':
        volume.create_directory(mode=cfg['-'][1])
    volume.apply_size()

    dirs = list(cfg.keys())
    for d in ['_rootdir','_map','-']:
        dirs.remove(d)
    dirs.sort()

    for d in dirs:
        if cfg[d][0] == 'copy':
            volume.copy_directory(cfg[d][1],path=d,mode=cfg[d][2])
        elif cfg[d][0] == 'dir':
            volume.create_directory(path=d,mode=cfg[d][1])
        elif cfg[d][0] == 'link':
            volume.symlink(cfg[d][1],d)

def update_users(ldap,base,basedir,volumes):
    obj = ldap3.ObjectDef(['posixAccount','systemQuotas'], ldap.connection)
    reader = ldap3.Reader(ldap.connection,obj,base)
    reader.search()
    for u in reader:
        log.debug('considering user {}'.format(u.uid.value))
        for q in u.quota:
            v,d = q.split(':')
            s = d.split(',')[0]

            if v not in volumes:
                log.debug('unknown volume type {}'.format(v))
                continue
            try:
                vv = UserVolume(basedir,volumes[v]['_rootdir'][0],u.uid.value,u.uid.value,'750','{}k'.format(s),uid=u.uidNumber.value,gid=u.gidNumber.value)
            except Exception as e:
                log.error('cannot create volume {} for user {}: {}'.format(v,u.uid.value,str(e)))
                continue
            
            createUserVolume(volumes[v],vv)
            if vv.is_student:
                h = '${STUDENTSERVER}'
            else:
                h = '${STAFFSERVER}'
            modify_volume(ldap,base,volumes[v]['_map'],u.uid.value,h,vv.path,root='${PREFIX}')



def main():
    CFG = '/etc/sysconfig/quadrat2'
    VOLCFG = '/etc/sysconfig/cephfs_volumes.config'
    parser = argparse.ArgumentParser(parents=[loggingParser()],
                                     description='create server volumes records in the LDAP.')

    group = parser.add_mutually_exclusive_group()
    group.add_argument('-v','--update-volumes',nargs='?',const=VOLCFG,metavar='VOLCFG',
                       help='Create group volumes specified in VOLCFG and update LDAP. By default VOLCFG={}'.format(VOLCFG))
    group.add_argument('-u','--update-user-volumes',action="store_true", default=False,
                       help="query ldap and create new user directories or modify user quotas")

    control = parser.add_argument_group("Control program behaviour")
    control.add_argument('-k','--keytab', 
                         action="store_true", default=False,
                         help="use host keytab to get krb5 credentials")
    control.add_argument("-y", "--yes",
                         action="store_true", default=False,
                         help="Never prompt for confirmation.")
    control.add_argument("-n", "--dry-run", "--no",
                         action="store_true", default=False,
                         help="Display what would be done, but make no changes.")

    parser.add_argument('--version',action='version',version=__version__)
    parser.add_argument('-c','--config',default=CFG,help='read configuration from file, default={}'.format(CFG))


    args = parser.parse_args()

    set_up_logging(args)

    # configure
    config = configparser.SafeConfigParser()
    config.read(args.config)

    for ldap in ('ldap', 'base'):
        setattr(args, ldap, config.get('ldaplist', ldap))

    ceph_basedir = pathlib.Path(config.get('cephfs','basedir'))
    ceph_nfsroot = config.get('cephfs','nfsroot')
    ceph_maps = {}
    for nismap,host in config.items('cephfs'):
        if nismap.startswith('auto.'):
            ceph_maps[nismap] = host

    ldap = LDAPConnection(dry_run=args.dry_run,yes=args.yes)
    if args.keytab:
        ldap.kinit()
    ldap.connect(args.ldap)

    if not ceph_basedir.is_dir():
        parser.error('cephfs base directory {} does not exist'.format(ceph_basedir))

    if args.update_volumes is not None:
        volumes = readGroupVolumes(args.update_volumes)
        for v in volumes:
            try:
                vol = Volume(ceph_basedir,volumes[v]['path'],
                             volumes[v]['user'],volumes[v]['group'],
                             volumes[v]['mode'],volumes[v]['size'])
                vol.create_directory()
                vol.apply_size()
                modify_volume(ldap,args.base,
                              volumes[v]['map'],
                              v,
                              ceph_maps[volumes[v]['map']],
                              pathlib.Path(volumes[v]['path']),
                              root='${PREFIX}')
            except Exception as e:
                logging.error('failed to create volume {}: {}'.format(v,str(e)))
    elif args.update_user_volumes:
        volumes = readUserVolumes(config)
        update_users(ldap,args.base,ceph_basedir,volumes)

if __name__ == '__main__':
    main ()
