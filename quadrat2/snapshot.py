import argparse
import sys
import datetime
import cephfs
from rados import Rados
import logging
import pathlib
from .logging import *
from . import __version__

log = logging.getLogger('quadrat2.snapshot')

def main():
    parser = argparse.ArgumentParser(parents=[loggingParser()],description='manage cephfs snapshots')
    parser.add_argument('directory',help="directory to be snapshotted")
    parser.add_argument('-n','--num-snapshots',default=10,metavar='N',type=int,help="keep N snapshots, default=10")
    parser.add_argument('-u','--ceph-user',default='client.admin',help='the ceph user, default=client.admin')
    parser.add_argument('-r','--ceph-root-dir',default='/pacific',help='cephfs root directory to mount')

    parser.add_argument('--version',action='version',version=__version__)

    args = parser.parse_args()

    set_up_logging(args)

    log.debug('connecting to ceph')
    try:
        rados = Rados(name=args.ceph_user,conffile = '/etc/ceph/ceph.conf')
        rados.connect()
    except Exception as e:
        log.error(e)
        sys.exit(1)

    log.debug('mounting {}'.format(args.ceph_root_dir))
    try:
        cfs = cephfs.LibCephFS(rados_inst=rados)
        cfs.mount(mount_root=args.ceph_root_dir.encode())
    except Exception as e:
        log.error(e)
        sys.exit(1)


    path = pathlib.Path(args.directory,'.snap')

    log.debug('get current snapshots')
    d = cfs.opendir(str(path))
    snapshots = []
    while True:
        entry = d.readdir()
        if entry is None:
            break
        # skip . and ..
        n = entry.d_name.decode()
        if n in ['.','..']:
            continue
        if entry.is_dir() and n.startswith('@GMT'):
            n = entry.d_name.decode()
            p = path/n
            snapshots.append(n)
    d.close()

    snapshots.sort(reverse=True)

    sname = datetime.datetime.utcnow().strftime('@GMT-%y.%m.%d-%H.%M.%S') 
    spath = str(path/sname)

    log.info('creating snapshot {}'.format(spath))
    
    if sname in snapshots:
        log.warning('snapshot {} exists already'.format(spath))
        sys.exit(1)

    cfs.mkdir(spath,0o755)

    for sname in snapshots[max(0,args.num_snapshots-1):]:
        spath = str(path/sname)
        log.info('removing snapshot {}'.format(spath))

        cfs.rmdir(spath)

    cfs.unmount()

if __name__ == '__main__':
    main ()
