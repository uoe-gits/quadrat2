__all__ = ['set_up_logging','loggingParser']

import logging, logging.handlers
import argparse

def loggingParser(add_help=False):
    parser = argparse.ArgumentParser(add_help=add_help)
    group = parser.add_argument_group('logging options')
    group.add_argument("--verbose", action="store_true",default=False,
                       help="Display verbose descriptions of actions taken.")
    group.add_argument("--quiet",action="store_true",default=False,
                       help="only display warnings or errors")
    group.add_argument("--syslog",action="store_true",default=False,
                       help="Send logging information to syslog")
    return parser


def set_up_logging(cmdargs):
    """Sets up logging for this run.

    If running as a service, we tend to be quieter than if we are running
    at the command line.  The  --syslog  option sets up an independent
    logging channel which is always noisier.
    """

    logger = logging.getLogger("quadrat2")
    # Set overall logger level to INFO
    logger.setLevel(logging.DEBUG)

    # Set a default level for the stderr logging
    level = logging.INFO
    if cmdargs.verbose:
        level = logging.DEBUG
    if cmdargs.quiet:
        level = logging.WARNING

    formatter = logging.Formatter('%(name)s - %(levelname)s: %(message)s')
    if cmdargs.syslog:
        syslog = logging.handlers.SysLogHandler(address='/dev/log')
        syslog.setFormatter(formatter)
        syslog.setLevel(logging.INFO)
        logger.addHandler(syslog)

    errout = logging.StreamHandler()
    errout.setFormatter(formatter)
    errout.setLevel(level)
    logger.addHandler(errout)

