import argparse
import configparser

import logging
from .logging import *
from .users import CentralUsers, User
from .ldap import LDAPConnection

log = logging.getLogger('quadrat2.update_web')

# generate data file using
# cd /disk/atlantic/web3
# du -s ?/* > /tmp/web3.data

def main():
    CFG = '/etc/sysconfig/quadrat2'

    parser = argparse.ArgumentParser(parents=[loggingParser()],
                                     prog='update_web',
                                     description='update web quota')
    parser.add_argument('web',help="name of file containing current web usage")

    control = parser.add_argument_group("Control program behaviour")
    control.add_argument('-k','--keytab', 
                         action="store_true", default=False,
                         help="use host keytab to get krb5 credentials")
    control.add_argument("-y", "--yes",
                         action="store_true", default=False,
                         help="Never prompt for confirmation.")
    control.add_argument("-n", "--dry-run", "--no",
                         action="store_true", default=False,
                         help="Display what would be done, but make no changes.")

    parser.add_argument('-c','--config',default=CFG,help='read configuration from file, default={}'.format(CFG))
    args = parser.parse_args()

    set_up_logging(args)

    # configure
    config = configparser.SafeConfigParser()
    config.read(args.config)

    for ldap in ('ldap', 'base'):
        setattr(args, ldap, config.get('ldaplist', ldap))
    
    ldap = LDAPConnection(dry_run=args.dry_run,yes=args.yes)
    if args.keytab:
        ldap.kinit()
    ldap.connect(args.ldap)

    central = LDAPConnection()
    central.connect(config.get('ldaplist', 'centralldap'),anonymous=True)
    cbase = config.get('ldaplist', 'centralbase')
    cUsers = CentralUsers(central,cbase)

    with open(args.web,'r') as infile:
        for line in infile.readlines():
            q,u = line.split()
            u = u.split('/')[1]
            size = '{}G'.format(max(int(q)//1e6+1,2.5))
            
            try:
                cuser = cUsers(u)
            except:
                log.warning('No such user {}'.format(u))
                continue

            user = User(ldap,args.base,cuser)
            user.set_quota('dotweb','2.5G')
            user.set_quota('web',size)
            user.commit()

if __name__ == '__main__':
    main()
