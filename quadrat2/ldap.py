__all__ = ['LDAPConnection']

import ldap3,ssl
import logging
import sys
import socket
import pathlib
import tempfile
import os
import subprocess

log = logging.getLogger('quadrat2.ldap')

class LDAPConnection:
    def __init__(self,dry_run=False,yes=False):
        self.dry_run = dry_run
        self.yes = yes

        self._connection = None

    @property
    def connection(self):
        return self._connection

    @property
    def dry_run(self):
        return self._dry_run
    @dry_run.setter
    def dry_run(self,v):
        self._dry_run = bool(v)
    @property
    def yes(self):
        return self._yes
    @yes.setter
    def yes(self,v):
        self._yes = bool(v)

    def kinit(self):
        """Use the host keytab to get krb5 credentials in to a temporary file.

        Returns the handle for the temporary file so that it can be held open
        for the duration of its usage.
        """

        thishost = socket.getfqdn()
        kinit = pathlib.Path('/usr/bin/kinit')
        if not kinit.exists():
            kinit = pathlib.Path('/usr/kerberos/bin/kinit')

        try:
            self._kcache = tempfile.NamedTemporaryFile(prefix='krb5cc_quadrat_')
            os.environ['KRB5CCNAME'] = self._kcache.name

            code = subprocess.call([kinit, '-k', '-t', '/etc/krb5.keytab', 
                                   'host/' + thishost])
            if code != 0:
                raise RuntimeError ('kinit returned exit code %d' % code)
        except Exception as detail:
            log.error('Cannot get credentials with kinit: %s' % (detail))
            sys.exit(1)

    def connect(self,ldapuri, anonymous=False):
        log.debug('connecting to ldap {}'.format(ldapuri))
        tls = ldap3.Tls(validate=ssl.CERT_NONE, version=ssl.PROTOCOL_TLSv1_2)
        server = ldap3.Server(ldapuri, use_ssl=True, tls=tls)
        if anonymous:
            self._connection = ldap3.Connection(server)
        else:
            self._connection = ldap3.Connection(
                server, authentication=ldap3.SASL, sasl_mechanism=ldap3.KERBEROS)
        try:
            self._connection.bind()
        except Exception as e:
            log.error('failed to connect to {}: {}'.format(ldapuri,e))
            sys.exit(1)

    def confirm(self,prompt = 'Proceed [y/N]: '):
        if self.dry_run:
            log.debug(prompt)
            return False
        if not self.yes:
            answer = input(prompt)
            if answer.lower() not in ('y', 'yes'):
                return False
        return True

