import argparse
import configparser
import pathlib,shutil,os

import logging
from .logging import *
from .ldap import LDAPConnection
from .users import *
from .configfiles import readUserVolumes, readGroupVolumes
from .volumes import userVolumeName
from .homesmap import delete_volume
from . import __version__

log = logging.getLogger('quadrat2.cull')


EDUNI_EXPIRED_TYPE = ['400', '500', '600', '700', '800',
                      '900', '910', '1000', '1100', ]

class GeosUsers(Users):
    OBJ = ['account','posixAccount','systemQuotas']

def cull_directory(source,archive,dry_run):
    if source.exists():
        log.debug('archiving {} to {}'.format(source,archive))
        if archive.exists():
            raise RuntimeError('archive {} already exists'.format(archive))
            return
        if not dry_run:
            if not archive.parent.exists():
                archive.parent.mkdir(parents=True,exist_ok=True)
            source.rename(archive)
            archive.touch()

def cull_user(gUsers,u,volumes,basedir,archivedir,cull_dir=None):
    log.info('culling user {}'.format(u.OA_uid))

    if hasattr(u,'quota'):
        u = u.entry_writable()
        for q in u.quota:
            qVolume = q.split(':')[0]
            # a user always needs a home and scratch directory
            if cull_dir is not None and cull_dir not in ['home','scratch']:
                if cull_dir in ['web','dotweb']:
                    # if either web or dotweb is specified cull both
                    if qVolume not in ['web','dotweb']:
                        continue
                elif qVolume != cull_dir:
                    continue
            if qVolume not in volumes:
                log.warning('unkown quota type {}'.format(qVolume))
                continue
            v = volumes[qVolume]
            root = v['_rootdir'][0]
            # make relative path
            if root.startswith('/'):
                root = root[1:]
            vname = userVolumeName (pathlib.Path(root),u.OA_uid.value)
            try:
                cull_directory(basedir/vname,archivedir/vname,gUsers.ldap.dry_run)
            except Exception as e:
                log.error('failed to cull user {}: {}'.format(u.OA_uid,e))
                return

            if cull_dir is not None:
                u.quota -= q

    if cull_dir is None or not hasattr(u,'quota'):
        u.entry_delete()

    prompt = None
    if u.entry_status == "Pending changes":
        prompt = '{}\nmodify {} [y/N]: '.format(str(u),u.OA_uid)
    elif u.entry_status == "Ready for deletion":
        prompt = "delete entry {} [y/N]".format(u.OA_uid)

    if prompt is not None:
        if gUsers.ldap.confirm(prompt):
            u.entry_commit_changes()
        if gUsers.ldap.connection.result['result'] != 0:
            log.error("{0[description]} - {0[message]}".format(gUsers.ldap.connection.result))

def cull_users(gUsers,cUsers,volumes,basedir,archivedir):
    for u in gUsers.all_users:
        cu = None
        try:
            cu = cUsers(u.OA_uid)
        except:
            pass

        if cu is not None and cu.eduniIDStatus.value not in EDUNI_EXPIRED_TYPE:
            #log.debug('not culling user {}'.format(u.OA_uid))
            continue

        cull_user(gUsers,u,volumes,basedir,archivedir)


def cull_volumes(ldap,base,volumes,basedir,archivedir,cull_volume=None):

    configured_directories = []
    directory_maps = {}
    for v in volumes:
        p = volumes[v]['path']
        if p.startswith ('/'):
            p = p[1:]
        p = basedir/p
        directory_maps[p.parent] = volumes[v]['map']
        configured_directories.append(p)        

    found_volume = False
    # check volume directories
    for vd in directory_maps:
        for d in vd.iterdir():
            if d not in configured_directories:
                if cull_volume is not None and cull_volume != d.name:
                    continue
                found_volume = True
                source = d
                archive = archivedir/d.relative_to(basedir)
                cull_directory(source,archive,dry_run=ldap.dry_run)

                delete_volume(ldap,base,directory_maps[source.parent],source.name)

    if cull_volume is not None and not found_volume:
        log.error('Could not find volume {}'.format(cull_volume))

def main():
    CFG = '/etc/sysconfig/quadrat2'
    VOLCFG = '/etc/sysconfig/cephfs_volumes.config'

    parser = argparse.ArgumentParser(parents=[loggingParser()],
                                     description='Cleans up unused group and user volumes')

    group = parser.add_mutually_exclusive_group()
    group.add_argument('-u','--cull-user',nargs='?',metavar='USER[:DIR]',const=True,
                       help='Cull user directories. If USER is specified then cull particular user even if they are still active. By default cull all inactive users. Only remove a particular directory if DIR is specified.')
    group.add_argument('-V','--cull-volume', nargs='?',metavar='VOLUME',const=True,
                       help="Cull unconfigured volumes. Only cull a particular volume when VOLUME is specified. By default cull all unconfigured volumes.")
    
    control = parser.add_argument_group("Control program behaviour")
    control.add_argument('-k','--keytab', 
                         action="store_true", default=False,
                         help="use host keytab to get krb5 credentials")
    control.add_argument("-y", "--yes",
                         action="store_true", default=False,
                         help="Never prompt for confirmation.")
    control.add_argument("-n", "--dry-run", "--no",
                         action="store_true", default=False,
                         help="Display what would be done, but make no changes.")

    parser.add_argument('--version',action='version',version=__version__)
    parser.add_argument('-c','--config',default=CFG,help='read configuration from file, default={}'.format(CFG))
    parser.add_argument('-v','--volume-config',default=VOLCFG,help='read volume configuration from file, default={}'.format(VOLCFG))

    args = parser.parse_args()

    set_up_logging(args)

    # configure
    config = configparser.SafeConfigParser()
    config.read(args.config)

    for ldap in ('ldap', 'base'):
        setattr(args, ldap, config.get('ldaplist', ldap))

    ceph_basedir = pathlib.Path(config.get('cephfs','basedir'))
    ceph_archivedir = pathlib.Path(config.get('cephfs','archivedir'))

    user_volumes = readUserVolumes(config)

    ldap = LDAPConnection(dry_run=args.dry_run,yes=args.yes)
    if args.keytab:
        ldap.kinit()
    ldap.connect(args.ldap)

    if not ceph_basedir.is_dir():
        parser.error('cephfs base directory {} does not exist'.format(ceph_basedir))
    if not ceph_archivedir.is_dir():
        parser.error('cephfs archive directory {} does not exist'.format(ceph_archivedir))

    if args.cull_user is True:
        gUsers = GeosUsers(ldap,args.base)

        central = LDAPConnection()
        central.connect(config.get('ldaplist', 'centralldap'),anonymous=True)
        cbase = config.get('ldaplist', 'centralbase')
        cUsers = CentralUsers(central,cbase)
        
        cull_users(gUsers,cUsers,user_volumes,ceph_basedir,ceph_archivedir)
    elif args.cull_user is not None:
        user = args.cull_user.split(':')
        if len(user)>1:
            cull_directory = user[1]
        else:
            cull_directory = None
        user = user[0]

        gUsers = GeosUsers(ldap,args.base)

        try:
            u = gUsers(user)
        except Exception as e:
            log.error('Could not cull user {}: {}'.format(user,e))

        cull_user(gUsers,u,user_volumes,ceph_basedir,ceph_archivedir,cull_dir=cull_directory)
    elif args.cull_volume:
        volumes = readGroupVolumes(args.volume_config)
        if args.cull_volume is True:
            cull_volume = None
        else:
            cull_volume = args.cull_volume

        cull_volumes(ldap,args.base,volumes,ceph_basedir,ceph_archivedir,cull_volume=cull_volume)
 


if __name__ == '__main__':
    main ()

