from pkg_resources import get_distribution, DistributionNotFound
try:
    __version__ = get_distribution('quadrat2').version
except DistributionNotFound as e:
    # package is not installed
    print (e)
    __version__ = '0.0'
    pass

