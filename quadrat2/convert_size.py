__all__ = ['convert_size']

SUFFIXES = 'bkmgt'

def s2f(s):
    """convert a suffix to a factor"""
    p = SUFFIXES.find(s.lower())
    if p == -1:
        raise RuntimeError('unknown suffix {}'.format(s))
    return 1024**p

def convert_size(size,inverse=False):
    if inverse:
        f = int(log2(size)//10)
        return '{:.2f}{}'.format(size/1024**f,SUFFIXES[f].upper())
    else:
        try:
            s = float(size)
            f = 1
        except:
            s = float(size[:-1])
            f = s2f(size[-1])
        return int(s*f)
