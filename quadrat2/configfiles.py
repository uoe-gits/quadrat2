__all__ = ['readGroupVolumes','readUserVolumes']

from configparser import SafeConfigParser

def readUserVolumes(cfg):
    """read user volume configuration file"""

    if isinstance(cfg,SafeConfigParser):
        config = cfg
    else:
        config = SafeConfigParser()
        config.read(cfg)

    volumes = {}
    for s in config.sections():
        if s.startswith('make_'):
            v = s[5:]
            volumes[v] = {}
            for d,o in config.items(s):
                if d == '_map':
                    volumes[v][d] = o
                else:
                    volumes[v][d] = o.split()
    return volumes
   
def readGroupVolumes(cfg):
    """read group volume configuration file"""
    
    if isinstance(cfg,SafeConfigParser):
        config = cfg
    else:
        config = SafeConfigParser()
        config.read(cfg)

    volumes = {}
    for v in config.sections():
        vol = {}
        for k,vv in config.items(v):
            vol[k] = vv
        volumes[v] = vol
    return volumes
    
