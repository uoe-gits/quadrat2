#!/bin/env python3

import configparser
import argparse
import pwd,grp
from pathlib import Path
import os
import stat
import posix1e
import sys
import distro
import logging

from .logging import *
from . import __version__

log = logging.getLogger('quadrat2.manage_user_projects')


VARIABLES   = ['basedir','user','group','project','ro_users','rw_users']
PERMISSIONS = {'ro_users':'r-x','rw_users':'rwx'}

class Directory:
    """a directory base class"""

    user_facl = None
    dir_facls = []

    def __init__(self,parent,name,user,group,mode,managed_acls=True):
        """initialisae basic directory

        Arguments
        ---------
        parent: the parent of the directory, can be either a Path object or another Directory object
        name: the name of the directory
        user: the user owning the directory
        group: the group owning the directory
        mode: mode of directory
        managed_acls: whether acls should be changed, defaults to True
        """
        self._name = name
        self._parent = parent
        self._path = None
        self._children = {}
        self._user = user
        self._group = group
        try:
            self._uid = pwd.getpwnam(user).pw_uid
        except:
            log.error('user {} does not exist'.format(user))
            raise
        try:
            self._gid = grp.getgrnam(group).gr_gid
        except:
            log.error('group {} does not exist'.format(group))
            raise
        self._mode = mode
        self._managed_acls = managed_acls

    @property
    def name(self):
        """
        the name of the directory
        """
        return self._name
    @property
    def parent(self):
        """
        the parent of the directory
        """
        return self._parent
    @property
    def path(self):
        """
        construct Path object from parent and name
        """
        if self._path is None:
            if isinstance(self.parent,Path):
                self._path = self.parent/self.name
            else:
                self._path = self.parent.path/self.name
        return self._path
    @property
    def children(self):
        """
        a dictionary mapping the name of child directories to their Directory object
        """
        return self._children
    @property
    def user(self):
        """
        the owning user
        """
        return self._user
    @property
    def group(self):
        """
        the owning group
        """
        return self._group
    @property
    def uid(self):
        """
        uid associated with user
        """
        return self._uid
    @property
    def gid(self):
        """
        gid associated with group
        """
        return self._gid
    @property
    def mode(self):
        """
        the mode
        """
        return self._mode
    @property
    def managed_acls(self):
        """
        whether acls should be changed or not
        """
        return self._managed_acls

    def get_facls(self):
        """
        get a list of facls
        """
        facls = []
        if self.managed_acls and self.user_facl is not None:
            users = []
            for u in self.get_users():
                if u not in users:
                    users.append(u)
                    facls.append('u:{}:{}'.format(u,self.user_facl))
        return facls

    def get_facls_string(self,always_update_acls=False):
        """
        create facl string from list of facls

        Arguments
        ---------
        always_update_acls: whether a facl string should be always created

        by default the facls string will only be created if the facls change. otherwise return None

        """
        # ACLs are totally confusing
        # see the following link for some explanation
        # https://unix.stackexchange.com/questions/147499/what-relationships-tie-acl-mask-and-standard-group-permission-on-a-file

        facls = self.get_facls()
        if len(facls)>0:
            facls.append('m::rwx')
            facls += self.dir_facls
            changed = always_update_acls
            # check if facls have changed
            if self.path.exists():
                old_facls = posix1e.ACL(file=str(self.path)).to_any_text(separator=b',',options=posix1e.TEXT_ABBREVIATE).decode().split(',')
            else:
                old_facls = []

            if len(facls) != len(old_facls):
                log.debug('number of project users changed')
                changed = True

            if not changed:
                for f in facls:
                    if f not in old_facls:
                        log.debug('new user permision {}'.format(f))
                        changed = True
                for f in old_facls:
                    if f not in facls:
                        log.debug('old user permision {}'.format(f))
                        changed = True

            if changed:
                # add standard facls
                
                facls.sort()
                facls = ','.join(facls)
                return facls

    def get_users(self):
        """
        iterator over all users including users associated with children
        """
        for c in self.children:
            for u in self.children[c].get_users():
                yield u

    def tidy_up(self,dry_run=False):
        """
        recursively remove unmanaged directories
        
        Arguments
        ---------
        dry_run: when set to True only report what is going to happen
        """
        
        # check if a directory needs to be tidied away
        if not self.path.exists():
            return
        for p in self.path.iterdir():
            if p.name not in self.children:
                if p.is_dir():
                    is_empty = True
                    for root, dirs, files in os.walk(p,topdown=False):
                        for d in dirs:
                            d = Path(root,d)
                            if d.is_symlink():
                                log.debug('removing link {}'.format(d))
                                if not dry_run:
                                    d.unlink()
                            else:
                                log.debug('removing directory {}'.format(d))
                                if not dry_run:
                                    d.rmdir()
                        for f in files:
                            f = Path(root,f)
                            if f.is_symlink():
                                log.debug('removing link {}'.format(f))
                                if not dry_run:
                                    f.unlink()
                            else:
                                log.warning('unexpected file {}'.format(f))
                                is_empty = False
                        if not is_empty:
                            break
                    else:
                        log.debug('removing directory {}'.format(p))
                        if not dry_run:
                            p.rmdir()
                else:
                    log.warning('unexpected file {}'.format(p))
        # now tidy up all children
        for c in self.children:
            self.children[c].tidy_up(dry_run=dry_run)

    def make_it(self,dry_run=False, always_update_acls=False, facls = None):
        """
        recursively create directories and apply acls
        
        Arguments
        ---------
        dry_run: when set to True only report what is going to happen
        always_update_acls: whether a facl string should be always created
        facls: when set to facls string use this instead of computing it
        """
        if not self.path.exists():
            log.debug('creating directory {}'.format(self.path))
            log.debug('with access {}:{} {:o}'.format(self.user,self.group,self.mode))
            if not dry_run:
                self.path.mkdir()
                os.chmod(self.path,self.mode)
                os.chown(self.path,self.uid,self.gid)
        if facls is None:
            facls = self.get_facls_string(always_update_acls = always_update_acls)
        if facls is not None:
            log.debug('applying facls {} to {}'.format(facls,self.path))
            if not dry_run:
                acl = posix1e.ACL(text=facls)
                acl.applyto(str(self.path),posix1e.ACL_TYPE_DEFAULT)
                acl.applyto(str(self.path))

        # now create all children
        for c in self.children:
            self.children[c].make_it(dry_run=dry_run, always_update_acls = always_update_acls)

class BaseDirectory(Directory):
    """a base directory

    named users get only execute permissions, ie they can traverse the directory but not list it
    the owning user gets read/execute permissions
    """

    user_facl = '--x'
    dir_facls = ['u::r-x','g::---','o::---']

    def __init__(self,parent,name,user,group,managed_acls=True):
        """initialisae basic directory

        Arguments
        ---------
        parent: the parent of the directory, can be either a Path object or another Directory object
        name: the name of the directory
        user: the user owning the directory
        group: the group owning the directory
        managed_acls: whether acls should be changed, defaults to True
        """
        super().__init__(parent,name,user,group,stat.S_IRUSR|stat.S_IXUSR,managed_acls=managed_acls)
        self._project_dir = None

    @property
    def project_dir(self):
        """
        the directory object where projects are kept
        """
        if self._project_dir is None:
            self._project_dir = self.add_directory(self.user)
        return self._project_dir

    def add_directory(self,name):
        """
        add new directory

        Arguments
        ---------
        name: the name of the directory to be added
        
        the new directory inherits user and group for the parent
        """
        if name not in self.children:
            self.children[name] = BaseDirectory(self,name,self.user,self.group)
        return self.children[name]

    def add_project(self,name):
        """
        add a new project
        
        Arguments
        ---------
        name: the name of the project to be added
        """
        if name not in self.project_dir.children:
            self.project_dir.children[name] = ProjectDirectory(self.project_dir,name,self.user)
        else:
            log.warning('project {} already exists'.format(name))
        return self.project_dir.children[name]
        
    def add_ro_user(self,project,user):
        """
        add a read-only user to a project
        
        Arguments
        ---------
        project: the name of the project to which the user is added
        user: the user name
        """
        self.add_user(project,user,rw=False)
    def add_rw_user(self,project,user):
        """
        add a read-write user to a project
        
        Arguments
        ---------
        project: the name of the project to which the user is added
        user: the user name
        """
        self.add_user(project,user,rw=True)

    def add_user(self,project,user,rw=True):
        """
        add a user to a project
        
        Arguments
        ---------
        project: the name of the project to which the user is added
        user: the user name
        rw: user has read/write permissions. default True
        """
        if project not in self.project_dir.children:
            log.error('project {} does not exist'.format(project))
            return
        if not user in self.children:
            self.children[user] = UserDirectory(self,user)

        prj = self.project_dir.children[project]

        self.children[user].projects[project] = prj.path
        if rw:
            prj.rw_users.add(user)
        else:
            prj.ro_users.add(user)

class ProjectDirectory(Directory):
    """a project directory containing project data"""
    dir_facls = ['u::rwx','g::---','o::---']

    def __init__(self,parent,name,user):
        """initialisae project directory

        Arguments
        ---------
        parent: the parent of the directory, can be either a Path object or another Directory object
        name: the name of the directory
        user: the user owning the directory
        """
        super().__init__(parent,name,user,user,stat.S_IRUSR|stat.S_IWUSR,stat.S_IXUSR)
        self._ro_users = set()
        self._rw_users = set()

    @property
    def ro_users(self):
        """
        a set containing the read-only users
        """
        return self._ro_users
    @property
    def rw_users(self):
        """
        a set containing the read-write users
        """
        return self._rw_users

    def get_users(self):
        """
        iterator over all users including users associated with children
        """
        for u in self.ro_users:
            yield u
        for u in self.rw_users:
            yield u

    def get_facls(self):
        """
        get a list of facls
        """
        facls = []
        for u in self.rw_users:
            facls.append('u:{}:rwx'.format(u))
        for u in self.ro_users:
            if u in self.rw_users:
                log.debug('user {} is already a rw user'.format(u))
            else:
                facls.append('u:{}:r-x'.format(u))
        return facls

    def make_it(self,dry_run=False, always_update_acls=False, facls = None):
        """
        recursively create directories and apply acls
        
        Arguments
        ---------
        dry_run: when set to True only report what is going to happen
        always_update_acls: whether a facl string should be always created
        facls: when set to facls string use this instead of computing it
        """
        facls = self.get_facls_string(always_update_acls = always_update_acls)
        super().make_it(dry_run=dry_run, always_update_acls=always_update_acls, facls=facls)

        if facls is not None:
            for root, dirs, files in os.walk(self.path):
                for f in files:
                    f=Path(root,f)
                    # get current user permissions
                    uperm = stat.filemode(f.stat().st_mode)[1:4]
                    # and update facls
                    ufacls = facls.replace('u::rwx','u::'+uperm).replace('m::rwx','m::'+uperm)
                    acl = posix1e.ACL(text=ufacls)
                    log.debug('applying facls {} to {}'.format(ufacls,f))
                    if not dry_run:
                        acl.applyto(str(f))
                acl = posix1e.ACL(text=facls)
                for d in dirs:
                    d=Path(root,d)
                    log.debug('applying facls {} to {}'.format(facls,d))
                    if not dry_run:
                        acl.applyto(str(d))
                        acl.applyto(str(d),posix1e.ACL_TYPE_DEFAULT)

    def tidy_up(self,dry_run=False):
        """
        recursively remove unmanaged directories
        
        Arguments
        ---------
        dry_run: when set to True only report what is going to happen

        Project data never gets tidied away automatically, so nothing to do.
        """
        return

class UserDirectory(Directory):
    """
    a project user directory containing symbolic links to the actual projects
    """
    
    def __init__(self,parent,user):
        """initialisae user directory

        Arguments
        ---------
        parent: the parent of the directory, can be either a Path object or another Directory object
        user: the user owning the directory
        """
        super().__init__(parent,user,user,user,stat.S_IRUSR|stat.S_IXUSR)

        self._projects = {}

    @property
    def projects(self):
        """
        dictionary of projects
        """
        return self._projects

    def get_users(self):
        """
        iterator over all users including users associated with children
        """
        return []

    def make_it(self,dry_run=False, always_update_acls=False):
        """
        recursively create directories and apply acls
        
        Arguments
        ---------
        dry_run: when set to True only report what is going to happen
        always_update_acls: whether a facl string should be always created
        facls: when set to facls string use this instead of computing it
        """
        super().make_it(dry_run=dry_run, always_update_acls=always_update_acls)
        # create project links
        for p in self.projects:
            link = self.path/p
            if not link.exists():
                log.debug('creating link {}->{}'.format(link,self.projects[p]))
                if not dry_run:
                    link.symlink_to(self.projects[p])

    def tidy_up(self,dry_run=False):
        """
        recursively remove unmanaged directories
        
        Arguments
        ---------
        dry_run: when set to True only report what is going to happen
        """
        if not self.path.exists():
            return
        # check if a directory needs to be tidied away
        for p in self.path.iterdir():
            if p.name not in self.projects:
                if p.is_symlink():
                    log.debug('removing link to project {}'.format(p))
                    if not dry_run:
                        p.unlink()
                else:
                    log.warning('unexpect file {}'.format(p))


class ProcessProjects:
    """
    class handling a collection of user projects
    """
    def __init__(self,no_check=False):
        """
        initialise ProcessProjects
        
        Arguments
        ---------
        no_check: default False

        by default the base directory is checked if it is on a mounted directory.
        """
        self._no_check = no_check
        self._base_directories = {}
        
    @property
    def no_check(self):
        """
        whether the base directory should be checked if it is mounted
        """
        return self._no_check

    def get_basedir(self,data):
        """
        get the basedir
        
        Arguments
        data: dictionary containing description of the project as read from config file
        """
        basedir = Path(data['basedir'])
        if basedir in self._base_directories:
            return self._base_directories[basedir]

        # check that the basedir is mounted locally
        mount_point = None
        if not self.no_check:
            is_mounted = False
            device = None
            # loop over entire basedir path
            for p in (basedir/'dummy').parents:
                if not p.exists():
                    continue
                if device is None:
                    device = p.stat().st_dev
                    previous = p
                    continue

                if p.stat().st_dev != device:
                    is_mounted = True
                    mount_point = previous
                    break
                device = p.stat().st_dev
                previous = p
            if not is_mounted:
                log.error('base directory {} appears to be part of the root file system'.format(basedir))
                return
            # check that basedir is a local mount
            local_mount = False
            with open('/etc/mtab','r') as mtab:
                for line in mtab.readlines():
                    line = line.split()
                    if line[1] == str(mount_point):
                        if not line[2].startswith('nfs'):
                            local_mount = True
                        break
                else:
                    log.error('could not find mount point {} in /etc/mtab'.format(basedir))
                    return
            if not local_mount:
                # silently return nothing to do here
                log.debug('{} is not on a local file system'.format(basedir))
                return

        if mount_point is None or basedir == mount_point:
            # no need to manage the ACLs of basedir as it is the mountpoint
            self._base_directories[basedir] = BaseDirectory(basedir.parent,basedir.name,data['user'],data['group'],managed_acls=False)
            return self._base_directories[basedir]
        else:
            base = None
            for p in basedir.relative_to(mount_point).parts:
                if base is None:
                    b = mount_point/p
                    if b not in self._base_directories:
                        self._base_directories[b] = BaseDirectory(mount_point,p,data['user'],data['group'])
                    base = self._base_directories[b]
                else:
                    base = base.add_directory(p)
            return base

    def add_project(self,cfg,name):
        """
        add a project

        Arguments
        ---------
        cfg: the config file object
        name: name of the project to process
        """
        log.debug('processing project {}'.format(name))
        data = {}
        project_ok = True
        for v in VARIABLES:
            if not cfg.has_option(name,v):
                project_ok = False
                continue
            data[v] = cfg.get(name,v)
        if not project_ok:
            log.warning('project [{}] is not complete'.format(name))
            return


        basedir = self.get_basedir(data)

        basedir.add_project(data['project'])

        for u in data['ro_users'].split():
            basedir.add_ro_user(data['project'],u)
        for u in data['rw_users'].split():
            basedir.add_rw_user(data['project'],u)

    def make_it(self,dry_run=False, always_update_acls=False):
        """
        recursively create directories and apply acls
        
        Arguments
        ---------
        dry_run: when set to True only report what is going to happen
        always_update_acls: whether a facl string should be always created
        facls: when set to facls string use this instead of computing it
        """
        for base in self._base_directories:
            self._base_directories[base].make_it(dry_run=dry_run,always_update_acls=always_update_acls)

    def tidy_up(self,dry_run=False):
        """
        recursively remove unmanaged directories
        
        Arguments
        ---------
        dry_run: when set to True only report what is going to happen
        """
        for base in self._base_directories:
            self._base_directories[base].tidy_up(dry_run=dry_run)


def main():
    if 'rhel' in distro.like().split():
        cfgfile = '/etc/sysconfig/user_projects.conf'            
    elif 'debian' in distro.like().split():
        cfgfile = '/etc/default/user_projects.conf'            

    parser = argparse.ArgumentParser(parents=[loggingParser()])
    parser.add_argument("-c","--config",default=cfgfile,metavar='FILE',help="read configuration from FILE, default: %s"%cfgfile)
    parser.add_argument("--disable-check-mount",action="store_true",default=False,help="do not check if basedirectory is a mount point")
    parser.add_argument("--always-update-acls",action="store_true",default=False,help="update ACLs of project directories even if they have not changed")
    parser.add_argument("--dry-run",action="store_true", default=False,help="Display what would be done, but make no changes.")

    parser.add_argument('--version',action='version',version=__version__)

    args = parser.parse_args()
    if args.dry_run:
        args.verbose = True

    set_up_logging(args)

    config = configparser.ConfigParser()
    config.read(args.config)

    projects = ProcessProjects(no_check=args.disable_check_mount)

    for section in config.sections():
        projects.add_project(config,section)

    projects.make_it(always_update_acls=args.always_update_acls,dry_run=args.dry_run)
    projects.tidy_up(dry_run=args.dry_run)
    
if __name__ == '__main__':
    main()
