# Quadrat: Quota, User and Directory Remote Administration Tool

Allow a system admin to store and modify information about users and
their quota and directories in the local LDAP.
On servers implements the changes requested in the LDAP.
Related commands also manage volumes and automount maps.
